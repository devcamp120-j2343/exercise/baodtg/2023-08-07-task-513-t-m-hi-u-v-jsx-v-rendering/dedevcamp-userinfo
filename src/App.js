import { gUserInfo } from "./info";


function App() {
  return (

    <div className="App">
      <h5> {gUserInfo.lastname} {gUserInfo.firstname} </h5>

      <img src={gUserInfo.avatar} width="300px"></img>

      <p>Tuổi của anh ấy {gUserInfo.age}</p>

      {gUserInfo.age < 35 ? "Anh ấy còn trẻ" : "Anh ấy đã già"}

      <ul>
        {
          gUserInfo.language.map((value, index) => {
            return <li key={index}>
              {value}
            </li>
          })
        }

      </ul>
    </div>
  );
}

export default App;
